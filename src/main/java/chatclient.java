/**
 * Created with IntelliJ IDEA.
 * User: inferno
 * Date: 3/3/13
 * Time: 11:03 PM
 * To change this template use File | Settings | File Templates.
 */

import java.net.*;
import java.io.*;

public class ChatClient {
    public static void main(String[] ar) {
        int serverPort = 1234; // здесь обязательно нужно указать порт к которому привязывается сервер.
        String address = "127.0.0.1"; // это IP-адрес компьютера, где исполняется наша серверная программа.
        // Здесь указан адрес того самого компьютера где будет исполняться и клиент.

        try {
            InetAddress ipAddress = InetAddress.getByName(address); // создаем объект который отображает вышеописанный IP-адрес.
            System.out.println("Any of you heard of a socket with IP address " + address + " and port " + serverPort + "?");
            Socket socket = new Socket(ipAddress, serverPort); // создаем сокет используя IP-адрес и порт сервера.
            System.out.println("Yes! I just got hold of the program.");

            // Берем входной и выходной потоки сокета, теперь можем получать и отсылать данные клиентом.
            InputStream sin = socket.getInputStream();
            OutputStream sout = socket.getOutputStream();
            BufferedReader br;
            BufferedWriter bw;

            // Конвертируем потоки в другой тип, чтоб легче обрабатывать текстовые сообщения.
//            DataInputStream in = new DataInputStream(sin);
//            DataOutputStream out = new DataOutputStream(sout);

            br = new BufferedReader(new InputStreamReader(sin));
            bw = new BufferedWriter(new OutputStreamWriter(sout));

            // Создаем поток для чтения с клавиатуры.
            BufferedReader keyboard = new BufferedReader(new InputStreamReader(System.in));
            String line = null;
            String fromserverline = null;
            System.out.println("Enter your name: ");
            String username = keyboard.readLine();
            System.out.println("Type in something and press enter. Will send it to the server and tell ya what it thinks.");
            System.out.println();

            while (true) {
                System.out.print(username+": ");
                line = keyboard.readLine(); // ждем пока пользователь введет что-то и нажмет кнопку Enter.
//                System.out.println("Sending this line to the server...");
                bw.write(username+": "+line+"\n"); // отсылаем введенную строку текста серверу.
                bw.flush(); // заставляем поток закончить передачу данных.

                if (br.ready()){
                    fromserverline = br.readLine();
                    System.out.println(fromserverline);
                    fromserverline = null;
                }

//                line = br.readLine(); // ждем пока сервер отошлет строку текста.
//                System.out.println("The server was very polite. It sent me this : " + line);
//                System.out.println("Looks like the server is pleased with us. Go ahead and enter more lines.");
//                System.out.println();
            }
        } catch (Exception x) {
            x.printStackTrace();
        }
    }
}