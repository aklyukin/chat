import java.net.*;
import java.io.*;
import java.util.concurrent.BlockingQueue;

/**
 * Created by andrey on 08/03/16.
 */
public class ClientWorker implements Runnable{

    private Socket client;
    private BlockingQueue<String> rqueue;
    private BlockingQueue<String> wqueue;


    public ClientWorker(Socket client, BlockingQueue rqueue, BlockingQueue wqueue){
        this.client = client;
        this.rqueue = rqueue;
        this.wqueue = wqueue;
    }

    public void run() {
        InputStream sin = null;
        OutputStream sout = null;
        BufferedReader br;
        BufferedWriter bw;
        try {
            sin = client.getInputStream();
            sout = client.getOutputStream();
        } catch (IOException e){
            System.out.println("in or out failed");
            System.exit(-1);
        }
//        DataInputStream in = new DataInputStream(sin);
//        DataOutputStream out = new DataOutputStream(sout);
        br = new BufferedReader(new InputStreamReader(sin));
        bw = new BufferedWriter(new OutputStreamWriter(sout));

        String line = null;
        while((br.ready()) || !rqueue.isEmpty()) {
            try {
                if (br.ready()){
                    line = br.readLine();
                    wqueue.put(line);
                    System.out.println("Client: " + line);
                    line = null;
                }

                while (!rqueue.isEmpty()){
                    bw.write(rqueue.take());
//                    bw.write("\n");
                    bw.flush();
                }
                System.out.println("Цикл 1 ");

            } catch (IOException | InterruptedException e) {
                System.out.println("Read failed");
                System.exit(-1);
            }

        }

    }
}
